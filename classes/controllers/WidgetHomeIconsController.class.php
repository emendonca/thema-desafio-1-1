<?php // Creating the widget 
class hi_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
		'hi_widget', 

		__('Home Icons', 'hi_widget_domain'), 

		array( 'description' => __( 'Widget para criação dos icones na home', 'hi_widget_domain' ), ) 
		);
	}

	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		$hi_icon = apply_filters( 'widget_icon', $instance['hi_icon'] );
		$hi_url = apply_filters( 'widget_url', $instance['hi_url'] );
		$hi_texto = apply_filters( 'widget_texto', $instance['hi_texto'] );
		echo $args['before_widget'];

		echo '
		<div class="widget col-md-4 text-center">
			<a href="'. $hi_url .'" title="'. $title .'" alt="'. $title .'">

				<span class="ico '. $hi_icon .'"></span>
				<h3>'. $title .'</h3>
				<p>'. $hi_texto .'</p>
			</a>
			
		</div>'
		;
		echo $args['after_widget'];
	}
			
	public function form( $instance ) {
		$title = $instance[ 'title' ];
		$hi_icon = $instance[ 'hi_icon' ];
		$hi_url = $instance[ 'hi_url' ];
		$hi_texto = $instance[ 'hi_texto' ];

		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Titulo:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>"  type="text" value="<?php echo esc_attr( $title ); ?>"/>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'hi_icon' ); ?>"><?php _e( 'Icone:' ); ?></label> 
			<select id="<?php echo $this->get_field_id( 'hi_icon' ); ?>" name="<?php echo $this->get_field_name( 'hi_icon' ); ?>">
				<option value='book' <?php if ( esc_attr( $hi_icon ) == 'book' ) { echo 'selected="selected"'; } ?>>Comunhão</option>
				<option value='heart' <?php if ( esc_attr( $hi_icon ) == 'heart' ) { echo 'selected="selected"'; } ?>>Relacionamento</option>
				<option value='users' <?php if ( esc_attr( $hi_icon ) == 'users' ) { echo 'selected="selected"'; } ?>>Missão</option>
			</select>

			<textareas class="widefat" id="<?php echo $this->get_field_id( 'hi_icon' ); ?>" name="<?php echo $this->get_field_name( 'hi_icon' ); ?>" rows="4" ><?php echo esc_attr( $hi_icon ); ?></textarea>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'hi_url' ); ?>"><?php _e( 'Url botão:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'hi_url' ); ?>" name="<?php echo $this->get_field_name( 'hi_url' ); ?>" type="text" value="<?php echo esc_attr( $hi_url ); ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'hi_texto' ); ?>"><?php _e( 'Texto:' ); ?></label> 
			<textarea class="widefat" id="<?php echo $this->get_field_id( 'hi_texto' ); ?>" name="<?php echo $this->get_field_name( 'hi_texto' ); ?>" rows="4" ><?php echo esc_attr( $hi_texto ); ?></textarea>
		</p>
		<?php 
	}
		
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['hi_icon'] = ( ! empty( $new_instance['hi_icon'] ) ) ? strip_tags( $new_instance['hi_icon'] ) : '';
		$instance['hi_url'] = ( ! empty( $new_instance['hi_url'] ) ) ? strip_tags( $new_instance['hi_url'] ) : '';
		$instance['hi_texto'] = ( ! empty( $new_instance['hi_texto'] ) ) ? strip_tags( $new_instance['hi_texto'] ) : '';
		return $instance;
	}
} // Class hi_widget ends here

// Register and load the widget
function wpb_load_HomeIcon() {
	register_widget( 'hi_widget' );
}
add_action( 'widgets_init', 'wpb_load_HomeIcon' );


