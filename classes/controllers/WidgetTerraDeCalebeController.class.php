<?php // Creating the widget 
class tmc_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
		'tmc_widget', 

		__('Desafio 1-1 - Second Video', 'tmc_widget_domain'), 

		array( 'description' => __( 'Widget para inserção do segundo vídeo na home', 'tmc_widget_domain' ), ) 
		);
	}

	public function widget( $args, $instance ) {
		$tmc_title = apply_filters( 'widget_title', $instance['tmc_title'] );
		$tmc_texto = apply_filters( 'widget_texto', $instance['tmc_texto'] );
		$tmc_player = apply_filters( 'widget_player', $instance['tmc_player'] );
		$video_id = str_replace("https://www.youtube.com/watch?v=", "", "$tmc_player");

		echo $args['before_widget'];
		echo '
		<div class="col-md-5">
			<h2>'. $tmc_title .'</h2>
			<p>'. $tmc_texto .'</p>
		</div>
		<div class="col-md-6 col-md-offset-1">
			<div id="playerYKeMjumboWIb"></div>
			<script type="text/javascript">
				jwplayer("playerYKeMjumboWIb").setup({
					file: "//www.youtube.com/watch?v='. $video_id .'",
					image: "http://img.youtube.com/vi/'. $video_id .'/hqdefault.jpg",
					width: "100%",
					autostart: false,
					aspectratio: "16:9"
				});
			</script>
		</div>'
		;
		echo $args['after_widget'];
	}
			
	public function form( $instance ) {
		$tmc_title = $instance[ 'tmc_title' ];
		$tmc_texto = $instance[ 'tmc_texto' ];
		$tmc_player = $instance[ 'tmc_player' ];

		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'tmc_title' ); ?>"><?php _e( 'Titulo:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'tmc_title' ); ?>" name="<?php echo $this->get_field_name( 'tmc_title' ); ?>"  type="text" value="<?php echo esc_attr( $tmc_title ); ?>"/>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'tmc_texto' ); ?>"><?php _e( 'Texto:' ); ?></label> 
			<textarea class="widefat" id="<?php echo $this->get_field_id( 'tmc_texto' ); ?>" name="<?php echo $this->get_field_name( 'tmc_texto' ); ?>" rows="4" ><?php echo esc_attr( $tmc_texto ); ?></textarea>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'tmc_player' ); ?>"><?php _e( 'Youtube URL:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'tmc_player' ); ?>" name="<?php echo $this->get_field_name( 'tmc_player' ); ?>"  type="text" value="<?php echo esc_attr( $tmc_player ); ?>"/>
		</p>

		<?php 
	}
		
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['tmc_title'] = ( ! empty( $new_instance['tmc_title'] ) ) ? strip_tags( $new_instance['tmc_title'] ) : '';
		$instance['tmc_texto'] = ( ! empty( $new_instance['tmc_texto'] ) ) ? strip_tags( $new_instance['tmc_texto'] ) : '';
		$instance['tmc_player'] = ( ! empty( $new_instance['tmc_player'] ) ) ? strip_tags( $new_instance['tmc_player'] ) : '';
		return $instance;
	}
} // Class tmc_widget ends here

// Register and load the widget
function wpb_load_widget() {
	register_widget( 'tmc_widget' );
}
add_action( 'widgets_init', 'wpb_load_widget' );


