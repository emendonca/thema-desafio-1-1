<?php
/*
Template Name: Front Page
*/
get_header();
?>

<?php if ( is_active_sidebar( 'pagina_inicial_icons' ) ) : ?>
<div class="icons">
	<div class="container">
		<div class="row text-center">
			<?php dynamic_sidebar( 'pagina_inicial_icons' ); ?>
		</div>
	</div>
</div>
<?php endif; ?>
<?php if ( is_active_sidebar( 'pagina_inicial_video02' ) ) : ?>
<div class="video2">
	<div class="container">
		<div class="row">
			<?php dynamic_sidebar( 'pagina_inicial_video02' ); ?>
		</div>
	</div>
</div>
<?php endif; ?>
<?php if ( is_active_sidebar( 'pagina_inicial_conteudo' ) ) : ?>
<div class="conteudo hidden-xs">
	<div class="container">
		<div class="row">
			<?php dynamic_sidebar( 'pagina_inicial_conteudo' ); ?>
		</div>
	</div>
</div>
<?php endif; ?>

<?php get_footer(); ?>